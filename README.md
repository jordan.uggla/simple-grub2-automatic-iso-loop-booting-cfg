# Simple grub2 automatic iso loop booting cfg

A simple grub cfg file that will search all volumes for a directory /boot-isos/ and generate a menu, at boot time, allowing you to boot any iso files found in that directory. The iso files must contain a valid /boot/grub/loopback.cfg .